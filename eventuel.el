;;; eventuel.el --- Share events in real time -*- lexical-binding: t -*-

;; Author: Tyler Grinn <tylergrinn@gmail.com>
;; Version: 0.1.0
;; Package-Requires: ((emacs "24.4") (request "0.3"))
;; File: eventu.el
;; Keywords: comm, tools
;; URL: https://gitlab.com/tygrdev/eventuel

;;; Commentary:

;; This package sends updates to a server whenever you clock in or out
;; or add a log entry to an org headline.

;;; Code:

;;;; Requirements

(require 'easy-mmode)
(require 'url-parse)
(require 'cl-lib)
(require 'request)


;;;; Customization variables
(defcustom eventuel-address "http://localhost:8377"
  "Location to communicate with the eventuel server."
  :type 'string
  :group 'eventuel)
(defcustom eventuel-prefix nil
  "Prefix key for the eventuel keymap."
  :type 'string
  :group 'eventuel)
(defcustom eventuel-send-clock t
  "Whether to send clock in and clock out events to the server."
  :type 'boolean
  :group 'eventuel)
(defcustom eventuel-send-logs t
  "Whether or not to send log entries to the server."
  :type 'boolean
  :group 'eventuel)
(defcustom eventuel-send-logs-only-clocked nil
  "Limit which log entries are sent to the server.

When true, only send log entries if the main entry the log is
filed under is currently clocked in."
  :type 'boolean
  :group 'eventuel)
(defcustom eventuel-send-todo t
  "Whether to send todo state changes to the server."
  :type 'boolean
  :group 'eventuel)
(defcustom eventuel-send-todo-only-clocked nil
  "Limit which todo state changes are sent to the server.

When true, only send todo state changes if the entry is currently
clocked in."
  :type 'boolean
  :group 'eventuel)
(defcustom eventuel-index t
  "Whether to show an index of all nodes publicly."
  :type 'boolean
  :group 'eventuel)
(defcustom eventuel-db-file "~/.eventuel.db"
  "File to store or load the lists of nodes and users from.

No clock or log entry will be stored in the database.  In order
to create an in-memory database that is refresh each time the
server is started, use ':memory:' as the db-file"
  :type 'string
  :group 'eventuel)

;;;; Utility expressions

(defconst eventuel--base (file-name-directory load-file-name))

(defun eventuel--parser ()
  "Parse responses from the eventuel server."
  (condition-case nil (json-read)
    (error
     (buffer-string))))

(defun eventuel--error ()
  "Function to handle errors from the eventuel server.")

(fset 'eventuel--error
  (cl-function
   (lambda (&key error-thrown &key data &allow-other-keys)
     (let ((err (cdr error-thrown)))
       (when (stringp err)
           (if (string= err "exited abnormally with code 7\n")
               (message "Eventuel server is not running at %s" eventuel-address)
             (message "Eventuel - unknown error: %s" err)))
       (when (listp err)
         (if (= (cadr err) 401)
             (message "Eventuel error - Unauthorized")
           (message "Eventual error (%d)\n%s" (cadr err) data)))))))

(defmacro eventuel--success (&rest body)
  "This is a utility macro for the request library.

In BODY, the variable `data' will be set to the JSON response
from the server"
  (list 'cl-function
        (append (list 'lambda (list '&key 'data '&allow-other-keys))
                body)))

(defun eventuel--auth ()
  "Retrieve authentication info for the eventuel server.

If not provided in an auth source search, this will ask for the
user and pass interactively."
  (let* ((url (url-generic-parse-url eventuel-address))
         (host (url-host url))
         (port (url-port url))
         (err-message (format "The authinfo file does not contain an entry for %s with port %d\n"
                              host port))
         (auth-source (car (auth-source-search :host host :port port)))
         (user (if auth-source (plist-get auth-source
                                          :user)
                 (read-string
                  (setq err-message (concat err-message "User: ")))))
         (pass (if auth-source (funcall (plist-get auth-source :secret))
                 (read-passwd (concat err-message user ", Pass: ")))))
    (list user pass)))

(defun eventuel--headers ()
  "Headers for the eventuel server for authentication and content type."
  `(("Content-Type" . "application/json")
    ("Authorization" . ,(concat "Basic " (base64-encode-string (apply 'format "%s:%s"
                                                                      (eventuel--auth)))))))


(defun eventuel--entry-info ()
  "Get information about the org entry related to a clock or log event."
  `(("title" . ,(org-entry-get nil "ITEM"))
    ("context" . ,(vconcat (org-get-outline-path)))
    ("author" . ,(user-login-name))
    ("tags" . ,(vconcat (org-get-tags)))))


;;;; Creating entries

(defun eventuel--send-clock-in ()
  "Send latest clock-in entry to the eventuel server."
  (let* ((clock-mark (car org-clock-history))
         (data (with-current-buffer (marker-buffer clock-mark)
                 (goto-char clock-mark)
                 (eventuel--entry-info))))
    (request (concat eventuel-address "/log")
      :type "POST"
      :parser: 'eventuel--parser
      :data (json-encode (append data
                                 `(("type" . "clock-in")
                                   ("time" . ,(format-time-string "%FT%T%z" org-clock-start-time)))))
      :headers (eventuel--headers)
      :error 'eventuel--error
      :success (eventuel--success (message "Eventuel - clock in event sent")))))

(defun eventuel--send-clock-out ()
  "Send clock out event to the eventuel server."
  (let* ((clock-mark (car org-clock-history))
         (data (with-current-buffer (marker-buffer clock-mark)
                 (goto-char clock-mark)
                 (eventuel--entry-info))))
    (request (concat eventuel-address "/log")
      :type "POST"
      :parser 'eventuel--parser
      :data (json-encode (append (eventuel--entry-info)
                                 `(("type" . "clock-out")
                                   ("time" . ,(format-time-string "%FT%T%z")))))
      :headers (eventuel--headers)
      :error 'eventuel--error
      :success (eventuel--success (message "Eventuel - clock out event sent")))))

(defun eventuel--send-log (orig &rest args)
  "Advice function to send log events to the eventuel server.

ORIG function is `org-store-log-note' and ARGS are any args that
should be passed along to it."
  (let ((text (buffer-string)))
    (while (string-match "\\`# .*\n[ \t\n]*" text)
      (setq text (replace-match "" t t text)))
    (when (string-match "\\s-+\\'" text)
      (setq text (replace-match "" t t text)))
    (apply orig args)
    (pp org-log-note-purpose)
    (when (and (not org-note-abort)
               (eq org-log-note-purpose 'note)
               (or (not eventuel-send-logs-only-clocked)
                   (string= (org-entry-get nil "ITEM") org-clock-current-task)))
      (request (concat eventuel-address "/log")
        :type "POST"
        :parser 'eventuel--parser
        :data (json-encode (append (eventuel--entry-info)
                                   `(("type" . "log")
                                     ("text" . ,text)
                                     ("time" . ,(format-time-string "%FT%T%z")))))
        :headers (eventuel--headers)
        :error 'eventuel--error
        :success (eventuel--success (message "Eventuel - log event sent"))))))

(defun eventuel--send-todo (to from)
  "Send todo state change to the eventuel server.

TO is the new state and FROM is the old state of the org entry."
  (when (and (not (string= to from))
             (or (not eventuel-send-todo-only-clocked)
                 (string= (org-entry-get nil "ITEM") org-clock-current-task)))
    (request (concat eventuel-address "/log")
      :type "POST"
      :parser 'eventuel--parser
      :data (json-encode (append (eventuel--entry-info)
                                 `(("type" . "state-change")
                                   ("from" . ,from)
                                   ("to" . ,to)
                                   ("time" . ,(format-time-string "%FT%T%z")))))
      :headers (eventuel--headers)
      :error 'eventuel--error
      :success (eventuel--success (message "Eventuel - state change sent"))))
  to)

;;;; Modifying nodes and users

(defun eventuel--read-node (prompt)
  "Use completing read to retrieve a node's name.

PROMPT will be used to prompt the user for a node."
  (message "Loading...")
  (let (nodes)
    (request (concat eventuel-address "/nodes")
      :type "GET"
      :parser 'eventuel--parser
      :sync t
      :error 'eventuel--error
      :headers (eventuel--headers)
      :success (eventuel--success (setq nodes (append data nil))))
    (completing-read prompt nodes nil t nil)))

(defun eventuel--read-user (node prompt)
  "Use completing read to retrieve a user from a node.

NODE should be a valid eventuel node.

PROMPT will be used to prompt for a user"
  (message "Loading...")
  (let (users)
    (request (concat eventuel-address "/nodes/" node "/users")
      :type "GET"
      :parser 'eventuel--parser
      :sync t
      :headers (eventuel--headers)
      :error 'eventuel--error
      :success (eventuel--success (setq users (append data nil))))
      (completing-read prompt users nil t nil)))


(defun eventuel-get-nodes ()
  "Print a list of nodes to the minibuffer."
  (interactive)
  (request (concat eventuel-address "/nodes")
    :type "GET"
    :headers (eventuel--headers)
    :parser 'eventuel--parser
    :error 'eventuel--error
    :success (eventuel--success (message "Nodes: %s" (mapconcat 'identity data " ")))))

(defun eventuel-create-node ()
  "Create a new node on the eventuel server."
  (interactive)
  (let ((name (read-string "New node: "))
        (tags (read-string "Match tags: "))
        (public (y-or-n-p "Public? "))
        (users [])
        (first t))
    (if (not public)
        (progn (while (y-or-n-p (if first (progn (setq first nil) "Add a user? ")
                                  "Add another user? "))
                 (setq users (vconcat users `((("user" . ,(read-string "Username: "))
                                               ("pass" . ,(read-passwd "Password: ")))))))))
    (request (concat eventuel-address "/nodes")
      :type "POST"
      :parser 'eventuel--parser
      :data (json-encode (append `(("name" . ,name)
                                   ("tags" . ,tags)
                                   ("users" . ,users))))
      :headers (eventuel--headers)
      :error 'eventuel--error
      :success (eventuel--success (message "Node created")))))


(defun eventuel-delete-node ()
  "Delete a node from the eventuel server."
  (interactive)
  (let ((node (eventuel--read-node "Delete node: ")))
    (request (concat eventuel-address "/nodes/" node)
      :type "DELETE"
      :parser 'eventuel--parser
      :headers (eventuel--headers)
      :error 'eventuel--error
      :success (eventuel--success (message "Node deleted.")))))


(defun eventuel-get-users ()
  "List users in a node."
  (interactive)
  (let ((node (eventuel--read-node "Get users for: ")))
    (request (concat eventuel-address "/nodes/" node "/users")
      :type "GET"
      :parser 'eventuel--parser
      :headers (eventuel--headers)
      :error 'eventuel--error
      :success (eventuel--success (message "Users: %s" (mapconcat 'identity data " "))))))


(defun eventuel-create-user ()
  "Add a user to a node."
  (interactive)
  (let ((node (eventuel--read-node "Create user for node: "))
        (user (read-string "Username: "))
        (pass (read-passwd "Password: ")))
    (request (concat eventuel-address "/nodes/" node "/users")
      :type "PUT"
      :parser 'eventuel--parser
      :headers (eventuel--headers)
      :data (json-encode `(("user" . ,user)
                           ("pass" . ,pass)))
      :error 'eventuel--error
      :success (eventuel--success (message "User added")))))

(defun eventuel-delete-user ()
  "Delete a user from a node."
  (interactive)
  (let* ((message "Delete user from ")
         (node (eventuel--read-node message))
         (user (eventuel--read-user node (concat message node ": "))))
    (request (concat eventuel-address "/nodes/" node "/users/" user)
      :type "DELETE"
      :parser 'eventuel--parser
      :headers (eventuel--headers)
      :error 'eventuel--error
      :success (eventuel--success (message "User deleted")))))

(defun eventuel-set-password ()
  "Set a password for a user in a node."
  (interactive)
  (let* ((message "Set password from ")
         (node (eventuel--read-node message))
         (user (eventuel--read-user node
                                    (setq message (concat message node " for
"))))
         (pass (read-passwd (concat message user ": "))))
    (request (concat eventuel-address "/nodes/" node "/users/" user)
      :type "PATCH"
      :parser 'eventuel--parser
      :headers (eventuel--headers)
      :data (json-encode `(("pass" . ,pass)))
      :error 'eventuel--error
      :success (eventuel--success (message "Done %s" (request-response-status-code response))))))


;;;; Managing a local server

(defvar eventuel--server-process nil)
(defvar eventuel--server-buffer nil)

(defun eventuel-server-start ()
  "Start a local instance of the eventuel server.

Server can be run using either NodeJS or Docker"
  (interactive)
  (if (not (null eventuel--server-process))
      (message "Stop the running server first with \"M-x eventuel-server-stop\"")

    (let* ((executor (completing-read "Executor: " '("node" "docker") nil t nil))
           (auth (eventuel--auth))
           (server (concat eventuel--base "server"))
           (port (number-to-string (url-port (url-generic-parse-url eventuel-address))))
           (db (concat (getenv "HOME") "/.eventuel.db"))
           (index (if eventuel-index "true" "false"))
           (command (if (string= executor "node")
                        (concat
                         "npm ci --prefix " server
                         " && PORT=" port
                         " DB_FILENAME=" eventuel-db-file
                         " MAIN_USER=" (car auth)
                         " MAIN_PASS=" (cadr auth)
                         " INDEX_NODES=" index
                         " node " server)
                      (concat
                       "docker build -t eventuel-server " server
                       " && docker run"
                       " --restart always"
                       " --name eventuel-server"
                       " --publish \"" port ":" port "\""
                       " --volume " eventuel-db-file ":/eventuel.db"
                       " --env PORT=" port
                       " --env MAIN_USER=" (car auth)
                       " --env MAIN_PASS=" (cadr auth)
                       " --env INDEX_NODES=" index
                       " eventuel-server"))))
      (unless (buffer-live-p eventuel--server-buffer)
          (setq eventuel--server-buffer  (generate-new-buffer "Eventuel Server")))
      (display-buffer eventuel--server-buffer)
      (unless (file-exists-p eventuel-db-file)
        (with-temp-buffer (write-file eventuel-db-file)))
      (setq eventuel--server-process
            (start-process-shell-command "Eventuel Server" eventuel--server-buffer command)))))


(defun eventuel-server-stop (&optional executor)
  "Stop a local instance of the eventuel server.

EXECUTOR can be either 'node' or 'docker'.  If called
interactively, it will prompt the user for one"
  (interactive)
  (if (null executor)
      (setq executor (completing-read "Executor: " '("node" "docker") nil t nil)))
  (if (buffer-live-p eventuel--server-buffer) (display-buffer eventuel--server-buffer))
  (if (string= executor "docker")
      (progn
        (shell-command "docker stop eventuel-server" eventuel--server-buffer)
        (shell-command "docker rm eventuel-server" eventuel--server-buffer)))
  (when (not (null eventuel--server-process))
    (if (process-live-p eventuel--server-process)
        (kill-process eventuel--server-process))
    (delete-process eventuel--server-process))
  (setq eventuel--server-process nil))

;;;; eventuel-mode

(defun eventuel--check-server (&optional callback)
  "Check that the server is running.

This will prompt to start the server if the host is 'localhost'.

CALLBACK is called if the server is already running."
  (request (concat eventuel-address "/verify")
    :type "GET"
    :error (cl-function
            (lambda (&rest args &allow-other-keys)
              (if (and (string=
                        (url-host (url-generic-parse-url eventuel-address))
                        "localhost")
                       (y-or-n-p
                        "Eventuel server not running, would you like to start it? "))
                  (eventuel-server-start)
                (message "Eventual server unreachable at %s" eventuel-address))))
    :success (eventuel--success
              (message "Eventuel server is running")
              (and callback (funcall callback)))))

(defun eventuel--check-nodes ()
  "Check that the server has at least one node.

This will prompt the user to create a new node if one does not exist."
  (request (concat eventuel-address "/nodes")
    :type "GET"
    :headers (eventuel--headers)
    :parser 'eventuel--parser
    :error 'eventuel--error
    :success (eventuel--success
              (and (null (append data nil))
                   (y-or-n-p "No nodes exist on the server, create one? ")
                   (eventuel-create-node)))))

(defvar eventuel-mode-map (make-sparse-keymap)
  "Keymap for managing nodes and users on the eventuel server.")

(defvar eventuel-command-map
  (easy-mmode-define-keymap
   (mapcar (lambda (k) (cons (kbd (car k)) (cdr k)))
           '(("n n" . eventuel-get-nodes)
             ("n c" . eventuel-create-node)
             ("n d" . eventuel-delete-node)
             ("u u" . eventuel-get-users)
             ("u c" . eventuel-create-user)
             ("u d" . eventuel-delete-user)
             ("u p" . eventuel-set-password))))
  "Postfix keymap to be used in `eventuel-mode-map'.")


;;;###autoload
(define-minor-mode eventuel-mode
  "Toggles sending emacs events to an external server."
  nil
  :global t
  :lighter " eventuel"
  :keymap eventuel-mode-map
  (if eventuel-mode
      (progn
        (when eventuel-prefix
          (setcdr eventuel-mode-map nil) ;; Remove previous prefix binding
          (define-key eventuel-mode-map (kbd eventuel-prefix) eventuel-command-map))
        (eventuel--check-server 'eventuel--check-nodes)
        (if eventuel-send-clock
            (progn (add-hook 'org-clock-in-hook #'eventuel--send-clock-in)
                   (add-hook 'org-clock-out-hook #'eventuel--send-clock-out)))
        (if eventuel-send-logs
            (advice-add 'org-store-log-note :around #'eventuel--send-log))
        (if eventuel-send-todo
            (add-hook 'org-todo-get-default-hook #'eventuel--send-todo)))
    (advice-remove 'org-store-log-note #'eventuel--send-log)
    (remove-hook 'org-clock-in-hook #'eventuel--send-clock-in)
    (remove-hook 'org-clock-out-hook #'eventuel--send-clock-out)
    (remove-hook 'org-todo-get-default-hook #'eventuel--send-todo)))

(provide 'eventuel)
;;; eventuel.el ends here
