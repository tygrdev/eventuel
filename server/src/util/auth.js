const basicAuth = require('express-basic-auth');
const bcrypt = require('bcrypt');
const nodes = require('../models/nodes');
const users = require('../models/users');

const { MAIN_USER, MAIN_PASS } = process.env;

exports.mainAuth = basicAuth({ users: { [MAIN_USER]: MAIN_PASS } });

exports.nodeAuth = async (req, res, next) => {
  const { node } = req.params;
  const isPublic = await nodes.isPublic(node);
  const authorizer = async (user, pass, cb) => {
    if (isPublic) {
      return cb(null, true);
    }
    if (
      basicAuth.safeCompare(user, MAIN_USER) &&
      basicAuth.safeCompare(pass, MAIN_PASS)
    ) {
      return cb(null, true);
    }
    if (!(await users.exists(node, user))) {
      return cb(null, false);
    }

    const hash = await users.getHash(node, user);
    cb(null, await bcrypt.compare(pass, hash));
  };
  return basicAuth({
    authorizer,
    authorizeAsync: true,
    challenge: true,
  })(req, res, next);
};
