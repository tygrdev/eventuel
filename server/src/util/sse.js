exports.default = (handler) => (req, res) => {
  res.writeHead(200, {
    'Content-Type': 'text/event-stream',
    'Cache-Control': 'no-cache',
    Connection: 'keep-alive',
  });

  handler(
    {
      send: (data) => res.write(`data: ${JSON.stringify(data)}\n\n`),
      onClose: (cb) => res.on('close', cb),
    },
    req
  );
};
