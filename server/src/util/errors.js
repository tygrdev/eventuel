class MalformedInputError extends Error {
  constructor(message) {
    super(message);
    this.name = this.constructor.name;
  }
}
exports.MalformedInputError = MalformedInputError;

class ConflictError extends Error {
  constructor(message) {
    super(message);
    this.name = this.constructor.name;
  }
}
exports.ConflictError = ConflictError;
