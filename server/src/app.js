const path = require('path');
const express = require('express');
const { ConflictError, MalformedInputError } = require('./util/errors');
const { mainAuth } = require('./util/auth');
const nodesRouter = require('./router/nodes').default;
const log = require('./log').default;

const app = express();
exports.default = app;

app.use(express.static(path.resolve(__dirname, 'web')), express.json());

app.get('/verify', (_req, res) => res.sendStatus(200));

app.post('/log', mainAuth, (req, res, next) => {
  const entry = req.body;
  console.log(entry);
  if (!entry.type || !entry.time || !entry.title || !entry.tags) {
    return next(new MalformedInputError());
  }

  log.emit('entry', entry);
  return res.sendStatus(200);
});

app.use('/nodes', nodesRouter);

// Global error handler
app.use((err, _req, res, _next) => {
  console.error(err);

  res.type('txt');

  if (err instanceof MalformedInputError) {
    return res.status(400).send(err.message || 'Invalid input');
  }
  if (err instanceof ConflictError) {
    return res.status(409).send(err.message || 'Conflict error');
  }
  return res.status(500).send('Unknown error occured, check server logs');
});
