const nodeList = document.getElementById('nodes');

fetch('/nodes')
  .then((res) => res.json())
  .then((nodes) => {
    const h2 = document.createElement('h2');
    h2.appendChild(document.createTextNode('Nodes'));
    nodeList.appendChild(h2);

    const ul = document.createElement('ul');
    nodeList.appendChild(ul);

    for (const node of nodes) {
      const li = document.createElement('li');
      const a = document.createElement('a');
      a.appendChild(document.createTextNode(node));
      a.addEventListener('click', () => {
        window.open(
          `nodes/${node}`,
          `Org Log - ${node}`,
          'top=0,width=510,height=200,menubar=no,status=no'
        );
      });
      li.appendChild(a);
      ul.appendChild(li);
    }
  });
