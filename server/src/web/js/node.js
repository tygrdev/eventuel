const node = location.pathname.split('/').pop();
const nodeEl = document.getElementById('node');

const log = new EventSource(`${location.pathname}/stream`);
log.onmessage = (event) => onEntryReceived(JSON.parse(event.data));

init();

function onEntryReceived(entry) {
  console.log('Entry received', entry);

  const entryEl = document.createElement('div');
  entryEl.classList.add('entry');

  let type;

  switch (entry.type) {
    case 'clock-in':
      type = 'CLOCK IN';
      break;
    case 'clock-out':
      type = 'CLOCK OUT';
      break;
    case 'log':
      type = 'LOG ENTRY';
      break;
    case 'state-change':
      type = 'STATE CHANGE';
      break;
  }

  const authorEl = document.createElement('h4');
  authorEl.classList.add('author');
  authorEl.appendChild(document.createTextNode('@' + entry.author));
  entryEl.appendChild(authorEl);

  const typeEl = document.createElement('h4');
  typeEl.classList.add('type');
  typeEl.appendChild(document.createTextNode(type));
  entryEl.appendChild(typeEl);

  const datetime = new Date(entry.time);

  const dateEl = document.createElement('h4');
  dateEl.classList.add('date');
  const dateOptions = {
    weekday: 'short',
    month: 'short',
    day: 'numeric',
    year: 'numeric',
  };
  dateEl.appendChild(
    document.createTextNode(datetime.toLocaleDateString('en-US', dateOptions))
  );
  entryEl.appendChild(dateEl);

  const timeEl = document.createElement('h4');
  timeEl.classList.add('time');
  const timeOptions = {
    timeZone: Intl.DateTimeFormat().resolvedOptions().timeZone,
  };
  timeEl.appendChild(
    document.createTextNode(datetime.toLocaleTimeString('en-US', timeOptions))
  );
  entryEl.appendChild(timeEl);

  if (entry.context.length > 0) {
    const contextEl = document.createElement('h4');
    contextEl.classList.add('context');
    contextEl.appendChild(
      document.createTextNode(entry.context.join(' / ') + ' /')
    );
    entryEl.appendChild(contextEl);
  }

  const titleEl = document.createElement('h4');
  titleEl.classList.add('title');
  titleEl.appendChild(document.createTextNode(entry.title));
  entryEl.appendChild(titleEl);

  if (entry.type === 'log') {
    const textEl = document.createElement('pre');
    textEl.classList.add('text');
    textEl.appendChild(document.createTextNode(entry.text));
    entryEl.appendChild(textEl);
  }

  if (entry.type === 'state-change') {
    const changeStateEl = document.createElement('div');
    changeStateEl.classList.add('state-change');

    const toState = document.createElement('h2');
    toState.classList.add('primary');
    toState.appendChild(document.createTextNode(entry.to));
    changeStateEl.appendChild(toState);

    if (entry.from) {
      const from = document.createElement('h4');

      const fromText = document.createElement('span');
      fromText.appendChild(document.createTextNode('from:'));
      fromText.style.marginRight = '8px';
      from.appendChild(fromText);

      const fromState = document.createElement('span');
      fromState.classList.add('primary');
      fromState.appendChild(document.createTextNode(entry.from));
      from.appendChild(fromState);

      changeStateEl.appendChild(from);
    }

    entryEl.appendChild(changeStateEl);
  }

  nodeEl.appendChild(entryEl);
  window.scrollTo(0, document.body.scrollHeight);
}

function init() {
  const intro = document.createElement('h2');
  const introSpan = document.createElement('span');
  introSpan.appendChild(document.createTextNode('Eventuel -- viewing node: '));
  intro.appendChild(introSpan);
  const nodeSpan = document.createElement('span');
  nodeSpan.classList.add('primary');
  nodeSpan.appendChild(document.createTextNode(node));
  intro.appendChild(nodeSpan);
  nodeEl.appendChild(intro);

  setTimeout(() => {
    const noMessages = document.createElement('h4');
    noMessages.appendChild(document.createTextNode('No messages yet...'));
    nodeEl.appendChild(noMessages);
  }, 1000);
}
