const sqlite3 = require('sqlite3');

const { DB_FILENAME } = process.env;

const db = new sqlite3.Database(DB_FILENAME);

exports.sql = (strings, ...args) =>
  new Promise((resolve, reject) => {
    db.all(strings.join('?'), args, (err, rows) => {
      if (err) reject(err);
      else resolve(rows);
    });
  });
