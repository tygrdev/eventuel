const { Router } = require('express');
const { mainAuth } = require('../util/auth');
const { MalformedInputError } = require('../util/errors');
const users = require('../models/users');

const router = Router({ mergeParams: true });
exports.default = router;

router.use(mainAuth);

router.put('/', async (req, res, next) => {
  const user = req.body;

  if (!user.user || !user.pass) return next(new MalformedInputError());

  try {
    await users.createAll(req.params.node, [user]);
  } catch (err) {
    return next(err);
  }

  return res.sendStatus(200);
});

router.patch('/:user', async (req, res, next) => {
  const { pass } = req.body;
  if (!pass) return next(new MalformedInputError());

  try {
    await users.setPassword(req.params.node, req.params.user, pass);
  } catch (err) {
    return next(err);
  }

  return res.sendStatus(200);
});

router.delete('/:user', async (req, res, next) => {
  try {
    await users.remove(req.params.node, req.params.user);
  } catch (err) {
    return next(err);
  }
  return res.sendStatus(200);
});

router.get('/', async (req, res, next) => {
  try {
    return res.status(200).json(await users.getAll(req.params.node));
  } catch (err) {
    return next(err);
  }
});
